const balanceParser = (balance) => {
  try {
    return parseInt(balance).toFixed(2);
  } catch(e) {
    return false;
  }
};

const toNumber = (balance) => {
  try {
    return parseInt(balance);
  } catch(e) {
    return false;
  }
};

const capFirstLetter = (string) => {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

const capFirstLetterLowerOther = (string) => {
  return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
}

module.exports = {
  balanceParser,
  capFirstLetter,
  capFirstLetterLowerOther,
  toNumber,
}