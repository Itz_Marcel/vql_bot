require('dotenv').config();
const fs = require("fs");
const { readdirSync } = require('fs');
const clc = require('cli-color');
const Discord = require('discord.js');

const client = new Discord.Client();
const onMessage = require('./triggers/onMessage.js');

process.stdout.write("\u001b[3J\u001b[2J\u001b[1J");console.clear();

client.commands = new Discord.Collection();

const getDirectories = source =>
  readdirSync(source, {
    withFileTypes: true
  })
  .filter(dirent => dirent.isDirectory())
  .map(dirent => dirent.name)

const dirs = getDirectories('./commands');

client.__modules = dirs;
client.__commands = [];

dirs.forEach(dir => {
  const commandFiles = fs
    .readdirSync(`./commands/${dir}`)
    .filter((file) => file.endsWith(".js"));

  for (const file of commandFiles) {
    const command = require(`./commands/${dir}/${file}`);
    command.client = client;
    command.module = dir;
    client.__commands.push(command.name);
    // set a new item in the Collection
    // with the key as the command name and the value as the exported module
    client.commands.set(command.name, command);

    if (command.aliases) {
      command.aliases.forEach(alias => {
        client.commands.set(alias, command);
      });
    }
  }
});

client.on('ready', () => {
  console.log(clc.magenta(`Logged in as ${client.user.username}`));
});

client.on('message', msg => {
  onMessage(client, msg);
});

client.on('rateLimit', console.log);

client.login(process.env.BOT_TOKEN);