const suggest = require('../commands/suggest.js');;
const mentions = require('../commands/mentions.js');;

module.exports = async (client, msg) => {
  if (msg.author.id === client.user.id || msg.author.bot) return;

  const prefix = '!';
  const content = msg.content;

  // check if there is actually a message send.
  if ((content.length - prefix.length) <= 0) return;  

  // check for mentions
  if (msg.mentions.everyone || msg.mentions.users.size > 0 || msg.mentions.roles.size > 0) {
    mentions(msg, client);
  }

  
  let messageWithoutPrefix = null;

  if (isPrefixUsed(content, prefix)) {
    messageWithoutPrefix = content.substring(prefix.length, content.length).split(' ');
  } else {
    messageWithoutPrefix = content.split(' ');
    messageWithoutPrefix.shift();
  }

  const command = messageWithoutPrefix.shift();
  const args = messageWithoutPrefix;

  if (client.commands.has(command)) {
    try {
      const commandExe = client.commands.get(command);

      let hasPermissions = false;

      if (commandExe.hasPermissions) {
        hasPermissions = await commandExe.hasPermissions(msg, args)
      } else {
        hasPermissions = true;
      };

      if (hasPermissions) {
        commandExe.execute(msg, args);
      } else {
        //const errorMessage = `<@${msg.member.user.id}>, you have no permission to use **${commandName}**.`;
        // client.sendError(errorMessage, msg, commandName, args)
        return;
      }

    } catch (error) {
      console.error(error);
      msg.channel.send('Error...');
    }
  }
}

const isPrefixUsed = (content, prefix) => content.substring(0, prefix.length) === prefix;