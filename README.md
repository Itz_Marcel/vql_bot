[Google docs documentation](https://docs.google.com/document/d/1Cbeno_rWptp5RPqukKG-n33lPr60fcFha2fJ8bnZgfA/edit#)

## Team deletion
A team is completely based on the role id. Deleting a role will not remove it from the database and thus stay in it forever. This will later be fixed with adding a trigger on role deletion. This is not 100% safe since a role can be deleted while the bot is offline.