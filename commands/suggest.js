const profanity = require( 'profanity-util', { substring: true } );

const CHANNEL_ID = '784066748625584169';
const WH_NAME = 'suggestions'

module.exports = async (msg, args, client) => {
  if (msg.author.bot) return;
  const content = args.join(' ');
  if (content.length < 2 || content.length > 1900) {
    return msg.channel.send('Suggestion too long or too short.');
  }

  const channel = msg.guild.channels.cache.get(CHANNEL_ID);

  const webhooks = await channel.fetchWebhooks();
  const webhookFound = webhooks.find(wh => wh.name === WH_NAME);

  if (!webhookFound) {
    try {
      return channel.createWebhook(WH_NAME)
        .then(webhook => {
          return sendSuggestion(msg, content, webhook);
        });
    } catch (e) {
      return msg.channel.send('Something went wrong, try again later or contact ItzMarcel#0001.');
    }
  } else {
    return sendSuggestion(msg, content, webhookFound);
  }
};

const sendSuggestion = (msg, content, webhook) => {
  const { username, discriminator } = msg.author;
  const [purifiedContent, swearwords] = profanity.purify(content, {obscureSymbol: '\\*'});

  if (swearwords.length > 0) {
    msg.delete();
  }

  webhook.send(purifiedContent, {
    username: `${username}#${discriminator}`,
    avatarURL: msg.author.avatarURL(),
  });
}


/*
Collection(1) [Map] {
  '784068864484966453' => Webhook {
    name: 'suggestions',
    avatar: null,
    id: '784068864484966453',
    type: 'Incoming',
    guildID: '784052403234275368',
    channelID: '784066748625584169',
    owner: ClientUser {
      id: '784052639205425233',
      system: null,
      locale: null,
      flags: null,
      username: 'Test bot',
      bot: true,
      discriminator: '9588',
      avatar: null,
      lastMessageID: null,
      lastMessageChannelID: null,
      verified: true,
      mfaEnabled: true,
      _typing: Map(0) {}
    }
  }
}
Created webhook [object Object]
*/

