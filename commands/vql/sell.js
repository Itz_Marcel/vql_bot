// !sell <ID> [amount]
// Default 1
const { MessageEmbed, ...Discord } = require('discord.js');
const balController = require('../../database/balController');
const teamController = require('../../database/teamController');
const itemController = require('../../database/itemController');
const util = require('../../Util/util');

module.exports = {
  name: 'sell',
  description: 'Sell an item.',
  guildOnly: true,
  commandHelp: `sell <ID> [amount]`,
  example: `sell CAR 3`,

  hasPermissions(msg) {
    return true;
  },

  async execute(msg, args) {
    const shortID = args[0].toUpperCase();
    let amount = args[1];

    if (!args || args.length <= 0) return msg.reply('No ID given to sell.');

    if (!amount) amount = 1;

    let item = null;

    const memberRoles = msg.member.roles.cache;
    let team = null;
    
    /*
     * In the DB we store only teams, not users.
     * Since a user can have multiple "roles", we have to search which role is in the database.
     * For simplicity sake and because performance is not key here, we take all the teams and 
     * look for a team that the user has as role.
     */
    try {
      const teams = await teamController.getAllTeams();
      team = teams.find(team => memberRoles.has(team.role_id));

      if (!team) throw 1;
      const { name, color } = memberRoles.find(role => role.id === team.role_id);

      team = {
        ...team,
        name,
        color,
      };
    } catch (e) {
      console.log(e);
      return msg.reply('Team not found.');
    }

    try {
      item = await itemController.getItemInInventory(team.role_id, shortID);
    } catch (e) {
      console.log(e);
      return msg.reply('something went wrong while looking up the item.');
    }

    if (!item) return msg.reply('ID not found.')
    if (!item.sellable) return msg.reply('Item can\'t be sold.');

    const priceToGet = util.toNumber(Math.abs(amount) * item.sell_value);

    if (!priceToGet) return msg.reply('Something went wrong converting the item price.');

    const teamBalance = util.toNumber(team.balance);
    if (!teamBalance) return msg.reply('Something went wrong converting your balance.');
    
    if (item.amount < amount){
      return msg.reply(`You only have that item ${item.amount} times.`);
    }

    const newBalance = teamBalance + priceToGet;

    try {
      await balController.addBalanceToTeam(amount, team.role_id, newBalance, item.id);
    } catch (e) {
      return msg.reply('Something went wrong selling your item.');
    }

    const sellEmbed = new MessageEmbed();
    sellEmbed
      .setTitle('Item sold successfully.')
      .setTimestamp()
      .setColor('#03adfc');

    return msg.channel.send(sellEmbed);
  }
}