// create an item
const ItemController = require('../../database/itemController');
const { MessageEmbed, ...Discord } = require('discord.js');

const FILTER_OPTIONS = { max: 1, time: 60000, errors: ['time']};
const FILTER = m => m.author.id === msg.author.id;
const FILTER_REACTION = (reaction, user) => user.id == msg.author.id && (reaction.emoji.name == '✅' || reaction.emoji.name == '❌');

module.exports = {
  name: 'createitem',
  description: 'Create an item',
  guildOnly: true,
  commandHelp: `createitem`,
  example: `createitem`,

  hasPermissions(msg) {
    return true;
  },

  async execute(msg, args) {
    if (msg.author.bot) return;
    let title = 'Item creation';
    let description = '';
    let informationField = {};

    const creationData = {};
    let save = true;

    const itemEmbed = new MessageEmbed()
        .setTitle(title)
        .addField('First step', `You have started with the item creation.
  You will be asked multiple question. You can decide to save or not at the end.
      
  What name would the item have?`)
    
    try {
      const embedMsg = await msg.channel.send(itemEmbed);
      const name = await awaitMsgReturnContent(msg);
      creationData.name = name;

      // do check
      title = `Creating ${name}`;
      description = `Name: ${name}`;
      informationField = {name: 'Next step', value: `Give a short ID for ${name} (max 25 characters)`};
      updateItemEmbed(itemEmbed, description, informationField, title);

      const shortId  = await awaitMsgReturnContent(msg, embedMsg, itemEmbed);
      creationData.shortId = shortId.toUpperCase();

      // do check
      description = `${name} — ${creationData.shortId}`
      informationField.value = `What would be a good short description of ${name}`;
      updateItemEmbed(itemEmbed, description, informationField);
      creationData.shortDesc = await awaitMsgReturnContent(msg, embedMsg, itemEmbed);

      // do check
      description += `\n${creationData.shortDesc}`;
      informationField.value = `Is ${name} buyable? React with the corresponding emoji.`;
      updateItemEmbed(itemEmbed, description, informationField);
      creationData.buyable = await awaitReactionReturnAnswer(msg, embedMsg, itemEmbed);

      if (creationData.buyable) {
        // do check
        informationField.value = `Buy price of ${name}`;
        updateItemEmbed(itemEmbed, description, informationField);
        creationData.buyValue = await awaitMsgReturnContent(msg, embedMsg, itemEmbed);
      }

      // do check
      description += `${creationData.buyValue ? `\nBuy price ${creationData.buyValue}`: ``}`
      informationField.value = `Is ${name} sellable? React with the corresponding emoji.`;
      updateItemEmbed(itemEmbed, description, informationField);
      creationData.sellable = await awaitReactionReturnAnswer(msg, embedMsg, itemEmbed);

      if (creationData.sellable) {
        // do check
        informationField.value = `Sell price of ${name}`;
        updateItemEmbed(itemEmbed, description, informationField);
        creationData.sellValue = await awaitMsgReturnContent(msg, embedMsg, itemEmbed);
      }

      // do check
      description += `${creationData.sellable ? `\nSell price ${creationData.sellValue}` : ``}`;
      informationField.value = 'Do you want to save your item? React with the corresponding emoji.';
      updateItemEmbed(itemEmbed, description, informationField);
      save = await awaitReactionReturnAnswer(msg, embedMsg, itemEmbed);
    } catch (e) {
      console.log(e);
      return msg.reply('Item creation has failed. Try again later or contact ItzMarcel#0001');
    }

    if (save) return saveItem(msg, creationData);

    const confirmMsg = await msg.channel.send('Are you sure you want to cancel? You will lose all progress.')
    .then(async (rmsg) => {
        await rmsg.react('✅');
        await rmsg.react('❌');
        return rmsg;
      });
    const collectedReactionConfirm = await confirmMsg.awaitReactions(FILTER_REACTION, { max: 1, time: 30000 });
    const confirm = collectedReactionConfirm.first().emoji.name == '✅' ? true : false;

    if (confirm) return msg.channel.send('Item creation cancelled successfully.');
    return saveItem(msg, creationData);
  }
}

const checkIfValid = (data) => {
  // under length limit
  // database limits
  // right types
  return true;
}

const saveItem = async (msg, creationData) => {
  try {
    await ItemController.createItem(creationData);
    const successEmbed = new MessageEmbed().setTitle(`Item ${creationData.name} created successfully.`).setColor('#12c429');
    return msg.channel.send(successEmbed)
  } catch (e) {
    const failEmbed = new MessageEmbed().setColor('#c42712');
    if (e.code === '23505') {
      failEmbed.setTitle('Failed to create item, already exists.');
      return msg.channel.send(failEmbed);
    }
    failEmbed.setTitle('Failed to create item.');
      return msg.channel.send(failEmbed);
  }
}

const updateItemEmbed = async (embed, desc, field, title) => {
  embed.setDescription(desc).spliceFields(0, 1, [field]);
  if (title) {
    embed.setTitle(title);
  }
}

const awaitMsgReturnContent = async (msg, embedMsg, itemEmbed) => {
  if (embedMsg && itemEmbed) await embedMsg.edit(itemEmbed);

  const inputMsg  = await msg.channel.awaitMessages(FILTER, FILTER_OPTIONS)
  const cleanInputContent = inputMsg.first().cleanContent;
  inputMsg.first().delete();
  return cleanInputContent;
}

const awaitReactionReturnAnswer = async (msg, embedMsg, itemEmbed) => {
  await embedMsg.edit(itemEmbed)
        .then(async (rmsg) => {
          await rmsg.react('✅');
          await rmsg.react('❌');
          return rmsg;
        });
  const collectedReaction = await embedMsg.awaitReactions(FILTER_REACTION, FILTER_OPTIONS);
  const userReaction = collectedReaction.first().emoji.name == '✅' ? true : false;
  await embedMsg.reactions.removeAll();
  return userReaction;
}

