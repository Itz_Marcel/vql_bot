// show balance !bal
// show team balance !bal @team

// Teamname's balance
// 2500

const TeamController = require('../../database/teamController');
const { MessageEmbed, ...Discord } = require('discord.js');

module.exports = {
  name: 'balance',
  description: 'Get your team balance.',
  guildOnly: true,
  commandHelp: `bal [@team]`,
  aliases: ['bal'],
  example: `bal`,

  async execute(msg, args) {
    const memberRoles = msg.member.roles.cache;
    let team = null;

    const roleMention = msg.mentions.roles.first();

    if (roleMention) {
      try {
        team = await TeamController.getTeamById(roleMention.id);
        if (!team) throw err;
        team = {
          ...team,
          name: roleMention.name,
          color: roleMention.color,
        };
      } catch (e) {
        return msg.reply('No team found.');
      }
    } else {
      /*
       * In the DB we store only teams, not users.
       * Since a user can have multiple "roles", we have to search which role is in the database.
       * For simplicity sake and because performance is not key here, we take all the teams and 
       * look for a team that the user has as role.
       */
      try {
        const teams = await TeamController.getAllTeams();
        team = teams.find(team => memberRoles.has(team.role_id));
  
        if (!team) throw err;
        const { name, color } = memberRoles.find(role => role.id === team.role_id);
  
        team = {
          ...team,
          name,
          color,
        };
      } catch (e) {
        return msg.reply('No team found.');
      }
    }

    const balanceEmbed = new MessageEmbed();
    balanceEmbed
      .setTitle(`${team.name}'s balance`)
      .setDescription(parseInt(team.balance).toFixed(2))
      .setColor(team.color)
      .setTimestamp();

    return msg.channel.send(balanceEmbed);
  }
}