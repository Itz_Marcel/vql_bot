// create a role for a team with empty inventory
const { createTeam } = require('../../database/teamController');
const { MessageEmbed, ...Discord } = require('discord.js');

module.exports = {
  name: 'createteam',
  description: 'Create a team with an empty inventory.',
  guildOnly: true,
  commandHelp: `createteam <name>`,
  example: `createteam teamname`,

  hasPermissions(msg) {
    return true;
  },

  async execute(msg, args) {
    let roleMention = msg.mentions.roles.first();

    if (!roleMention && args.length <= 0) return msg.reply(`Make sure to give a teamname \`${this.commandHelp}\``)

    if (!roleMention) {
      try {
        await msg.guild.roles.create({
          data: {
            name: args.join(' '),
          },
          reason: 'Created role with !createteam command.'
          }).then(newRole => roleMention = newRole);
      } catch (e) {
        console.log(e);
        return msg.reply('Failed to create a role for the team.');
      }
    }

    if (roleMention) {
      try {
        await createTeam(roleMention.id);
      } catch (e) {
        if (e.code === '23505') {
          return msg.reply ('Team already exists.');
        }
        return msg.reply('Failed to create team.')
      }
    }

    const createEmbed = new MessageEmbed();
    createEmbed
      .setTitle(`Team has been created.`)
      .setTimestamp()
      .setColor('#03adfc');

    return msg.channel.send(createEmbed);
  }
}