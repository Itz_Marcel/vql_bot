// !buy <ID> [amount]
// Default 1
const { MessageEmbed, ...Discord } = require('discord.js');
const itemController = require('../../database/itemController');
const teamController = require('../../database/teamController');
const util = require('../../Util/util');

module.exports = {
  name: 'buy',
  description: 'Buy an item.',
  guildOnly: true,
  commandHelp: `buy <ID> [amount]`,
  example: `buy CAR 3`,

  hasPermissions(msg) {
    return true;
  },

  async execute(msg, args) {
    const shortID = args[0].toUpperCase();
    let amount = args[1];

    if (!args || args.length <= 0) return msg.reply('No ID given to buy.');

    if (!amount) amount = 1;

    let item = null;

    try {
      item = await itemController.getItemWithShortID(shortID);
    } catch (e) {
      return msg.reply('something went wrong while looking up the item.');
    }

    if (!item) return msg.reply('ID not found.')
    if (!item.buyable) return msg.reply('Item can\'t be bought.');

    const priceToPay = util.toNumber(Math.abs(amount) * item.buy_value);

    if (!priceToPay) return msg.reply('Something went wrong converting the item price.');

    const memberRoles = msg.member.roles.cache;
    let team = null;
    
    /*
     * In the DB we store only teams, not users.
     * Since a user can have multiple "roles", we have to search which role is in the database.
     * For simplicity sake and because performance is not key here, we take all the teams and 
     * look for a team that the user has as role.
     */
    try {
      const teams = await teamController.getAllTeams();
      team = teams.find(team => memberRoles.has(team.role_id));

      if (!team) throw 1;
      const { name, color } = memberRoles.find(role => role.id === team.role_id);

      team = {
        ...team,
        name,
        color,
      };
    } catch (e) {
      return msg.reply('Team not found.');
    }

    const teamBalance = util.toNumber(team.balance);
    if (!teamBalance) return msg.reply('Something went wrong converting your balance.');
    
    if (teamBalance < priceToPay) return msg.reply('You don\'t have enough money to buy this.');

    const newBalance = teamBalance - priceToPay;

    try {
      await itemController.addItemToInventory(team.role_id, shortID, amount, newBalance);
    } catch (e) {
      return msg.reply('Something went wrong buying your item.');
    }

    const buyEmbed = new MessageEmbed();
    buyEmbed
      .setTitle('Item bought successfully.')
      .setTimestamp()
      .setColor('#03adfc');

    return msg.channel.send(buyEmbed);
  }
}