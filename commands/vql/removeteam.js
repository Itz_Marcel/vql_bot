// remove a role from the database
const { MessageEmbed, ...Discord } = require('discord.js');
const teamController = require('../../database/teamController');

module.exports = {
  name: 'removeteam',
  description: 'Remove a team.',
  guildOnly: true,
  commandHelp: `removeteam <@role/role id> [force]`,
  example: `removeteam @team force`,
  aliases: ['rmt'],

  hasPermissions(msg) {
    return true;
  },

  async execute(msg, args) {
    let roleMention = msg.mentions.roles.first();
    let roleID = null;

    const removeRole = args[1] === "force" ? true : false;

    if (!roleMention && args.length <= 0) return msg.reply(`Make sure to mention a teamname or give the role_id.\`${this.commandHelp}\``)

    if (!roleMention) {
      roleID = args[0];
    } else {
      roleID = roleMention.id;
    }

    try {
      const result = await teamController.removeTeam(roleID);
      if (roleMention && removeRole) roleMention.delete();
      if (!roleMention && removeRole) msg.guild.roles.cache.find(role => role.id === roleID)?.delete();
      
      if (result.rowCount === 0) {
        throw err;
      }
    } catch (e) {
      return msg.reply('Team not found.')
    }

    const removeEmbed = new MessageEmbed();
    removeEmbed
      .setTitle(`Team succesfully  removed`)
      .setTimestamp()
      .setColor('#03adfc');

    return msg.channel.send(removeEmbed);
  }
}