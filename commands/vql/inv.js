// show inventory !inv, !inv 2, !inv 3
// purge inventory !inv purge @team

// Teamnames inventory
// name - amount
// name - amount
// name - amount
// name - amount
// page x/x

const TeamController = require('../../database/teamController');
const InventoryController = require('../../database/inventoryController');
const { balanceParser, capFirstLetterLowerOther } = require('../../Util/util');
const { MessageEmbed, ...Discord } = require('discord.js');

module.exports = {
  name: 'inventory',
  description: 'Check your teams inventory',
  guildOnly: true,
  commandHelp: `inventory [page] [@team]`,
  aliases: ['inv'],
  example: `inventory 2`,

  async execute(msg, args) {
    const memberRoles = msg.member.roles.cache;
    let team = null;
    let inventory = null;

    const roleMention = msg.mentions.roles.first();

    let page = args[0];
    // Check if the first argument is a role name or if there is a page given.
    // If they do `inventory @team` it should return page 1 of the team
    if (!page || page.substring(0, 1) === '<') page = 1;

    if (roleMention) {
      // TODO: check if person is moderator.
      try {
        team = await TeamController.getTeamById(roleMention.id);
        if (!team) throw err;
        team = {
          ...team,
          name: roleMention.name,
          color: roleMention.color,
        };
      } catch (e) {
        return msg.reply('Team not found.');
      }
    } else {
      /*
       * In the DB we store only teams, not users.
       * Since a user can have multiple "roles", we have to search which role is in the database.
       * For simplicity sake and because performance is not key here, we take all the teams and 
       * look for a team that the user has as role.
       */
      try {
        const teams = await TeamController.getAllTeams();
        team = teams.find(team => memberRoles.has(team.role_id));
  
        if (!team) throw 1;
        const { name, color } = memberRoles.find(role => role.id === team.role_id);
  
        team = {
          ...team,
          name,
          color,
        };
      } catch (e) {
        return msg.reply('Team not found.');
      }
    }

    try {
      inventory = await InventoryController.getInventory(team.role_id, page);
    } catch (e) {
      return msg.reply('Error while getting inventory.');
    }

    const pages = Math.ceil(inventory.total/InventoryController.inventoryConfig.limit) || 1;
    if (page > pages) {
      return msg.reply(`That page does not exist, max page is ${pages}.`)
    }

    // They have no items and page limit is not exceeded.
    let reducedInventory = [{ name: 'No items', value: 'Feels bad man' }];

    if (inventory.data) {
      // This is not a good naming, but I had a brain malfunction. Help me.
      reducedInventory = inventory.data.map(item => { return { name: capFirstLetterLowerOther(item.name), value:  `\`${item.short_id.toUpperCase()}\` — ${item.amount}`}});
    }


    const invEmbed = new MessageEmbed();
    invEmbed
      .setTitle(`${team.name}'s inventory`)
      .setDescription(`Balance: ${balanceParser(team.balance)}`)
      .addFields(...reducedInventory)
      .setFooter(`Page ${page}/${pages}`)
      .setColor(team.color)
      .setTimestamp();

    return msg.channel.send(invEmbed);
  }
}