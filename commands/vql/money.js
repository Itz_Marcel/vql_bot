// give money
// remove money
// purge money !money purge @team
const { MessageEmbed, ...Discord } = require('discord.js');

module.exports = {
  name: 'money',
  description: 'Give or take money from a team.',
  guildOnly: true,
  commandHelp: `money <give/remove/purge> <@team> <amount>`,
  example: `money purge @team`,

  hasPermissions(msg) {
    return true;
  },

  async execute(msg, args) {
    const workInProgress = new MessageEmbed();
    workInProgress
      .setTitle(this.name)
      .setDescription(this.description)
      .setTimestamp()
      .setColor('#03adfc');

    return msg.channel.send(workInProgress);
  }
}