// show leaderboard !leaderboard
const BalController = require('../../database/balController');
const { balanceParser } = require('../../Util/util');
const { MessageEmbed, ...Discord } = require('discord.js');

module.exports = {
  name: 'leaderboard',
  description: 'Show the leaderboard.',
  guildOnly: true,
  commandHelp: `leaderboard`,
  aliases: ['lb'],
  example: `leaderboard`,

  async execute(msg, args) {
    let leaderboard = await BalController.getLeaderboard();

    const guildRoles = msg.guild.roles.cache;

    leaderboard = leaderboard.map(team => {
      const { name } = guildRoles.find(role => role.id === team.role_id);
      return {
        ...team,
        name,
      }
    });

    const workInProgress = new MessageEmbed();
    workInProgress
      .setTitle('Leaderboard')
      .setDescription(leaderboard.reduce(lbReducer, ''))
      .setTimestamp()
      .setColor('#03adfc');

    return msg.channel.send(workInProgress);
  }
}

const lbReducer = (acc, curr, index) => acc += `\n${index + 1}. ${curr.name} — ${balanceParser(curr.balance)}`;