// give items
// take items
// show all items

const { MessageEmbed, ...Discord } = require('discord.js');
const itemController = require('../../database/itemController');
const { capFirstLetterLowerOther, balanceParser } = require('../../Util/util');

module.exports = {
  name: 'items',
  description: 'Get all existing items or more information about the item.',
  guildOnly: true,
  commandHelp: `items [itemname]`,
  example: `items`,

  hasPermissions(msg) {
    return true;
  },

  async execute(msg, args) {
    const regex = RegExp(/[a-zA-Z]+/);
    if (args[0] && args[0].length > 1 && regex.test(args[0])) {
      return itemDetail(msg, args);
    }

    return itemsList(msg, args);
  }
}

const itemDetail = async (msg, args) => {
  const itemID = args[0].toUpperCase();

  let item = null;

  try {
    item = await itemController.getItemWithShortID(itemID);
  } catch (e) {
    return msg.reply('An error has occured. Contact ItzMarcel#0001 if you think its not your fault.');
  }
  
  if (!item) {
    return msg.reply('No item found with that ID.');
  }

  const { name, short_description, buy_value, sell_value, sellable, buyable, short_id } = item;
  
  const fields = [
    {
      name: `${buyable ? 'Buy price' : 'Unbuyable'}`,
      value: `${buyable ? balanceParser(buy_value) : '0'}`,
      inline: true,
    },
    {
      name: `${sellable ? 'Sell price' : 'Unsellable'}`,
      value: `${sellable ? balanceParser(sell_value) : '0'}`,
      inline: true,
    }
  ];

  const itemDetailEmbed = new MessageEmbed();
  itemDetailEmbed
    .setTitle(`${name} — \`${short_id}\``)
    .setDescription(short_description)
    .addFields(fields)
    .setColor('#b342f5')
    .setTimestamp();

  return msg.channel.send(itemDetailEmbed);
}

const itemsList = async (msg, args) => {
  const items = await itemController.getAllItems();

  let page = null;

  try {
    page = parseInt(args[0]) || 1;
  } catch (e) {
    page = 1;
  }

  const pages = Math.ceil(items.total/itemController.itemlistConfig.limit) || 1;
  if (page > pages) {
    return msg.reply(`That page does not exist, max page is ${pages}.`)
  }

  // They have no items and page limit is not exceeded.
  let reducedItemList = [{ name: 'No items', value: 'Feels bad man' }];

  if (items.data) {
    // This is not a good naming, but I had a brain malfunction. Help me.
    reducedItemList = items.data.map(item => { return { 
      name: `${capFirstLetterLowerOther(item.name)} — \`${item.short_id}\``, 
      value:  `${item.buyable ? `Buy price: ${item.buy_value}\n` : 'Unbuyable\n'}${item.sellable ? `Sell price: ${item.sell_value}` : 'Unsellable'}`}});
  }

  const itemlistEmbed = new MessageEmbed();
  itemlistEmbed
    .setTitle(`Item List`)
    .setDescription(`You can do \`!item <ID>\` for more information about an item.`)
    .addFields(...reducedItemList)
    .setFooter(`Page ${page}/${pages}`)
    .setColor('#b342f5')
    .setTimestamp();

  return msg.channel.send(itemlistEmbed);
}