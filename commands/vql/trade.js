// start trade !trade @team

// add/remove items

// accept/deny trade


// make channel
// show 2 columns with team names and added items
// let them add/remove things
// both accept/revoke/cancel

// !add <id> <amount> !remove <id> <amount>
// !add <amount> !remove <amount>
// !accept
// !revoke
// !cancel
const { MessageEmbed, ...Discord } = require('discord.js');

module.exports = {
  name: 'trade',
  description: 'Start trading with another team, this will create a seperate channel for your trade only.',
  guildOnly: true,
  commandHelp: `trade <@team>`,
  example: `trade @team`,

  hasPermissions(msg) {
    return true;
  },

  async execute(msg, args) {
    const workInProgress = new MessageEmbed();
    workInProgress
      .setTitle(this.name)
      .setDescription(this.description)
      .setTimestamp()
      .setColor('#03adfc');

    return msg.channel.send(workInProgress);
  }
}
