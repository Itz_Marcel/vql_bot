const { MessageEmbed } = require('discord.js');

module.exports = {
  name: 'settopic',
  description: 'Set the status from the bot.',
  guildOnly: true,
  commandHelp: `setstatus <status>`,
  example: `setstatus with code.`,

  hasPermissions(msg) {
    return msg.author.id === process.env.BOT_OWNER;
  },

  async execute(msg, args) {
    const ch = await msg.guild.channels.cache.find(channel => channel.id === '784052403234275371').fetch(true);
    console.log(ch.topic);
    
    console.log(ch.setTopic(args.join(' '), 'random reason')
      .then(channel => console.log(channel.topic))
      .catch(console.log))
  }
}