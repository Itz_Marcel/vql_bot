const { Util } = require('discord.js');
const CHANNEL_ID = '784076354097381446';
const WH_NAME = 'mentions'

module.exports = async (msg, client) => {
  if (msg.author.bot) return;

  const channel = msg.guild.channels.cache.get(CHANNEL_ID);

  const webhooks = await channel.fetchWebhooks();
  const webhookFound = webhooks.find(wh => wh.name === WH_NAME);

  if (!webhookFound) {
    try {
      return channel.createWebhook(WH_NAME)
        .then(webhook => {
          return sendWHMessage(msg, webhook);
        });
    } catch (e) {
      return msg.channel.send('Something went wrong, try again later or contact ItzMarcel#0001.');
    }
  } else {
    return sendWHMessage(msg, webhookFound);
  }
}

const sendWHMessage = (msg, webhook) => {
  const { everyone, users, roles } = msg.mentions;
  const { username, discriminator, id: userID } = msg.author;

  if (everyone || users.size > 0 || roles.size > 0) {
    webhook.send(msgWithoutPinging(msg), {
      username: `${username}#${discriminator} || ${userID}`,
      avatarURL: msg.author.avatarURL(),
    });
  }


  return;
}

const msgWithoutPinging = (msg) => {
  return msg.cleanContent;
}