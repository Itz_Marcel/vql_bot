const conn = require("./_conn");

const getAllTeams = async () => {
  const query = `select * from team`;
  return conn.query(query).then(res => res.rows);
};

const getTeamById = async (role_id) => {
  const query = `select * from team where role_id = $1`;
  return conn.query(query, [role_id]).then(res => res.rows[0]);
}

const createTeam = async (role_id, balance = 0) => {
  const query = `insert into team (role_id, balance)
                values ($1, $2)`
  return conn.query(query, [role_id, balance]);
}

const removeTeam = async (role_id) => {
  const query = `delete from team
                where role_id = $1`;
  return conn.query(query, [role_id]);
}

module.exports = {
  getAllTeams,
  getTeamById,
  createTeam,
  removeTeam
};
