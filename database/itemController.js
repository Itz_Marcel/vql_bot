const conn = require("./_conn");

const itemlistConfig = {
  limit: 6
}

const createItem = async ({ shortId, name, shortDesc, buyValue, sellValue, sellable, buyable } = {}) => {
  const query = `insert into item (short_id, name, short_description, buy_value, sell_value, sellable, buyable)
                values ($1, $2, $3, $4, $5, $6, $7)`;
  return conn.query(query, [shortId, name, shortDesc, buyValue || 0, sellValue || 0, sellable, buyable]).then(res => res.rows[0]);
};

const getAllItems = async (page = 1) => {
  const { limit } = itemlistConfig;
  // Offset should start at 0 but pages begin at 1.
  const offset = limit * (page - 1);

  const query = `select count(*) as total, (select JSON_AGG(row)
                  from (
                    select * from item limit $1 offset $2
                  
                  ) row) as data
                from item
                `;
  return conn.query(query, [limit, offset]).then(res => res.rows[0]);
}

const getItemWithShortID = async (shortId) => {
  const query = `select *
                from item
                where short_id = $1`;
  return conn.query(query, [shortId]).then(res => res.rows[0]);
}

const addItemToInventory = async (roleId, shortId, amount, newBalance) => {
  console.log(roleId, shortId);
  const query = `select *
                from team_item as ti
                inner join item as i on ti.item_id = i.id
                where team_id = $1 and short_id =  $2;`
  let itemInInventory = null;

  try {
    itemInInventory = await conn.query(query, [roleId, shortId]).then(res => res.rows[0]);
  } catch(e) {
    return false;
  }

  if (itemInInventory && itemInInventory.amount > 0) {
    return conn.query('select * from addInventoryItemAndTakeBalance($1, $2, $3, $4)', [amount, roleId, itemInInventory.id, newBalance]);
  }
  if (!itemInInventory) {
    return conn.query('select * from insertInventoryItemAndTakeBalance($1, $2, $3, $4)', [amount, roleId, shortId, newBalance]);
  }
}

const getItemInInventory = async (roleId, shortId) => {
  const query = `select ti.amount, i.*
                from team_item as ti
                inner join item as i on ti.item_id = i.id
                where team_id = $1 and short_id =  $2;`
  return conn.query(query, [roleId, shortId]).then(res => res.rows[0]);
}

module.exports = {
  createItem,
  getAllItems,
  itemlistConfig,
  getItemWithShortID,
  addItemToInventory,
  getItemInInventory
};
