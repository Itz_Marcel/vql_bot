const conn = require("./_conn");

const inventoryConfig = {
  limit: 6
}

const getInventory = async (role_id, page = 1) => {
  const { limit } = inventoryConfig;
  // Offset should start at 0 but pages begin at 1.
  const offset = limit * (page - 1);

  const query = `select count(*) as total, (select JSON_AGG(row)
                  from (
                      select i.*, ti.amount
                    from team as t
                    inner join team_item as ti on t.role_id = ti.team_id
                    inner join item as i on ti.item_id = i.id
                    where role_id = $1
                    LIMIT $2 OFFSET $3
                  ) row) as data
                  from team_item as ti
                  where team_id = $1`;
  return conn.query(query, [role_id, limit, offset]).then(res => res.rows[0]);
};

module.exports = {
  getInventory,
  inventoryConfig
};
