const conn = require("./_conn");

const getLeaderboard = async (limit = 10) => {
  const query = `select *
                from team
                order by balance asc
                limit $1`;
  return conn.query(query, [limit]).then(res => res.rows);
}

const addBalanceToTeam = async (amount, roleId, newBalance, itemId) => {
  return conn.query('select * from addBalanceAndRemoveItemFromInv($1, $2, $3, $4)', [amount, roleId, newBalance, itemId]);
}

module.exports = {
  getLeaderboard,
  addBalanceToTeam
};
