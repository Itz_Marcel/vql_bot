const conn = require("./_conn");

const getTime = async (id = 1) => {
  const query = `select * from time where id = $1;`;
  return conn.query(query, [id]).then(res => res.rows[0]);
};

const createTime = (time) => {
  const query = ``;

  return new Promise((resolve, reject) => {
    conn.query(query, [],
      (err, result) => {
        if (err) {
          reject(err);
        } else {
          resolve(result);
        }
      }
    )
  });
};

const setTime = (id, time) => {
  const query = `update time
    set timevalue = $1,
    startvalue = $1,
    updated_at = now()
    where id = $2`;

  return new Promise((resolve, reject) => {
    conn.query(query, [time, id],
      (err, result) => {
        if (err) {
          reject(err);
        } else {
          resolve(result);
        }
      }
    )
  });
};

const updateTime = (id, time) => {
  const query = 
  `update time
  set timevalue = $1,
  updated_at = now()
  where id = $2`;

  return new Promise((resolve, reject) => {
    conn.query(query, [time, id],
      (err, result) => {
        if (err) {
          reject(err);
        } else {
          resolve(result);
        }
      }
    )
  });
}

module.exports = {
  getTime,
  createTime,
  updateTime,
  setTime,
};