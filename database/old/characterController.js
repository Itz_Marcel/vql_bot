const conn = require("./_conn");

const getCharacters = async (memberID) => {
  const query = `select * from time where id = $1;`;
  return conn.query(query, [memberID]).then(res => res.rows);
};

const getOneCharacter = async (memberID) => {
  const query = `select * from time where id = $1;`;
  return conn.query(query, [memberID]).then(res => res.rows);
};

const createCharacter = (char) => {
  const query = ``;
  return conn.query(query, []);
};

const updateCharacter = (id, time) => {
  const query = ``;
  return conn.query(query, [time, id]);
};

module.exports = {
  getCharacters,
  createCharacter,
  updateCharacter,
  getOneCharacter,
};