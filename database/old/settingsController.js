const conn = require("./_conn");

/*
create table syserglobal(
  id serial primary key,
  status text,
  created_at timestamp,
  updated_at timestamp
)
*/

const getSettings = (guildid) => {
  const query = 
  `select *
  from guild
  where guildid = $1`;

  return new Promise((resolve, reject) => {
    conn.query(query, [guildid],
      (err, result) => {
        if (err) {
          reject(err);
        } else {
          resolve(result.rows[0]);
        }
      }
    )
  });
}

const setSettings = (guildid, settings) => {
  const query = 
  `update guild
  set settings = $1,
  prefix = $2,
  updated_at = now()
  where guildid = $3`;
  
  const {
    prefix,
    ...csettings
  } = settings;

  return new Promise((resolve, reject) => {
    conn.query(query, [csettings, prefix, guildid],
      (err, result) => {
        if (err) {
          reject(err);
        } else {
          resolve(result);
        }
      }
    )
  });
}

const initSettings = (guildid, settings, prefix) => {
  const query = 
  `insert into guild (guildid, settings, prefix, created_at, updated_at)
  values($1, $2, $3, now(), now())`;

  return new Promise((resolve, reject) => {
    conn.query(query, [guildid, settings, prefix],
      (err, result) => {
        if (err) {
          reject(err);
        } else {
          resolve(result);
        }
      }
    )
  });
}

module.exports = {
  getSettings,
  setSettings,
  initSettings,
};